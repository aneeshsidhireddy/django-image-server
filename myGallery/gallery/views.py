import base64
from django.shortcuts import render, redirect
from .models import Image, Emoji, EmotionAnalysisResult
from .forms import *
import cv2
import os
import numpy as np
from keras.models import Sequential
from keras.layers import Dense, Dropout, Flatten
from keras.layers import Conv2D
from keras.layers import MaxPooling2D
from django.shortcuts import render
from django.conf import settings
from django.core.files import File
from django.core.files.base import ContentFile
from django.core.files.temp import NamedTemporaryFile
import base64
# Create your views here.

# Define the file paths
model_weights_path = os.path.join(settings.BASE_DIR, 'gallery/model1.h5')
emotion_dict = {0: "Angry", 1: "Disgusted", 2: "Fearful", 3: "Happy", 4: "Neutral", 5: "Sad", 6: "Surprised"}
# emoji_folder = os.path.join(settings.BASE_DIR, 'recognition/emoji')

# Load the emotion model
emotion_model = Sequential()
emotion_model.add(Conv2D(32, kernel_size=(3, 3), activation='relu', input_shape=(48, 48, 1)))
emotion_model.add(Conv2D(64, kernel_size=(3, 3), activation='relu'))
emotion_model.add(MaxPooling2D(pool_size=(2, 2)))
emotion_model.add(Dropout(0.25))
emotion_model.add(Conv2D(128, kernel_size=(3, 3), activation='relu'))
emotion_model.add(MaxPooling2D(pool_size=(2, 2)))
emotion_model.add(Conv2D(128, kernel_size=(3, 3), activation='relu'))
emotion_model.add(MaxPooling2D(pool_size=(2, 2)))
emotion_model.add(Dropout(0.25))
emotion_model.add(Flatten())
emotion_model.add(Dense(1024, activation='relu'))
emotion_model.add(Dropout(0.5))
emotion_model.add(Dense(7, activation='softmax'))
emotion_model.load_weights(model_weights_path)


def recognize_emotion(image) -> str:
    # Save the image to a temporary file
    image_path = os.path.join(settings.BASE_DIR, settings.MEDIA_ROOT, 'pics', image.title)
    with open(image_path, 'wb') as f:
        f.write(image.photo.read())
    # Load the image and perform emotion recognition
    frame = cv2.imread(image_path)
    frame = cv2.resize(frame, (600, 500))
    gray_frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    # Load the face cascade classifier
    cascade_path = os.path.join(cv2.data.haarcascades, 'haarcascade_frontalface_default.xml')
    face_cascade = cv2.CascadeClassifier(cascade_path)
    num_faces = face_cascade.detectMultiScale(gray_frame, scaleFactor=1.3, minNeighbors=5)

    show_text = [0]  # Initialize show_text list

    for (x, y, w, h) in num_faces:
        cv2.rectangle(frame, (x, y-50), (x+w, y+h+10), (255, 0, 0), 2)
        roi_gray_frame = gray_frame[y:y + h, x:x + w]
        cropped_img = np.expand_dims(np.expand_dims(cv2.resize(roi_gray_frame, (48, 48)), -1), 0)
        prediction = emotion_model.predict(cropped_img)

        maxindex = int(np.argmax(prediction))
        cv2.putText(frame, emotion_dict[maxindex], (x+20, y-60), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 255, 255), 2, cv2.LINE_AA)
        show_text[0] = maxindex

    emotion = emotion_dict[show_text[0]].lower()

    return emotion

def analyse_image(request, id):
    image = Image.objects.get(id=id)
    generated_emotion = recognize_emotion(image)
    generated_emoji = Emoji.objects.get(title=generated_emotion)
    result = EmotionAnalysisResult.objects.create(orig_image=image, emotion=generated_emotion, emoji=generated_emoji)
    return redirect('image_list')

def image_list(request):
    results = [EmotionAnalysisResult.objects.last()]
    context = {'results': results}
    return render(request, "gallery/image_list.html", context)

def upload_image(request):
    if request.method == 'POST':
        form = ImageUploadForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            return redirect('analyse_image', id=form.instance.id)
    else:
        form = ImageUploadForm()
    context = {'form': form}
    return render(request, 'gallery/image_upload_form.html', context)

def convert_base64_to_file(base64_data):
    format, imgstr = base64_data.split(';base64,')
    ext = format.split('/')[-1]
    data = ContentFile(base64.b64decode(imgstr), name=f'captured_image.{ext}')
    return data


def capture_image(request):
    if request.method == 'POST':
        photo = convert_base64_to_file(request.FILES)
        form = Capture_Image(title = request.POST, photo = photo)
        if form.is_valid():
            form.save()
            return redirect('analyse_image', id=form.instance.id)
    else:
        return render(request, 'gallery/capture_image.html')


# def capture_image(request):
#     if request.method == 'POST':
#         image_data = request.POST.get('image_data')
#         # Convert the base64 image data to a file-like object
#         image_file = convert_base64_to_file(image_data)
#         # Create a new instance of the CapturedImage model and save the image
#         captured_image = Image(title ='capture_temp' ,image=image_file)
#         captured_image.save()
#         # Render a response or redirect to a success page
#         return redirect('analyse_image', id=)
#     else:
#         return render(request, 'capture_image.html')