from django import forms
from .models import Image, Capture_Image

class ImageUploadForm(forms.ModelForm):
    class Meta:
        model = Image
        fields = ['title', 'photo']


# class ImageCaptureForm(forms.ModelForm):
#     class Meta:
#         model = Capture_Image   
#         fields = ['title', 'photo']