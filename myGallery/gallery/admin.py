from django.contrib import admin
from .models import Image, Emoji, EmotionAnalysisResult
# Register your models here.
admin.site.register([Image, Emoji,EmotionAnalysisResult])